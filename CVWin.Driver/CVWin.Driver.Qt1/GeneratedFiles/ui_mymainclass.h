/********************************************************************************
** Form generated from reading UI file 'mymainclass.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYMAINCLASS_H
#define UI_MYMAINCLASS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyMainClassClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyMainClassClass)
    {
        if (MyMainClassClass->objectName().isEmpty())
            MyMainClassClass->setObjectName(QStringLiteral("MyMainClassClass"));
        MyMainClassClass->resize(600, 400);
        menuBar = new QMenuBar(MyMainClassClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        MyMainClassClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MyMainClassClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MyMainClassClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(MyMainClassClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        MyMainClassClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MyMainClassClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyMainClassClass->setStatusBar(statusBar);

        retranslateUi(MyMainClassClass);

        QMetaObject::connectSlotsByName(MyMainClassClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyMainClassClass)
    {
        MyMainClassClass->setWindowTitle(QApplication::translate("MyMainClassClass", "MyMainClass", 0));
    } // retranslateUi

};

namespace Ui {
    class MyMainClassClass: public Ui_MyMainClassClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYMAINCLASS_H
