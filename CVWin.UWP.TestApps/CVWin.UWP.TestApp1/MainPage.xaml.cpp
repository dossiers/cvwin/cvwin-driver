﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/highgui/highgui_winrt.hpp>
#include <iostream>

using namespace cv;
using namespace std;

using namespace CVWin_UWP_TestApp1;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Xaml::Media::Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
    InitializeComponent();


    //Mat img(200, 400, CV_8UC3, Scalar(255, 0, 0));
    //cout << img << endl;


    // Cf:
    // OpenCV HighGui module available for WinRT
    //     https ://msopentech.com/blog/2015/07/10/opencv-highgui-module-available-for-winrt/
    // WinRT support
    //     http://docs.opencv.org/trunk/d6/d2f/group__highgui__winrt.html#ga1c1224490daecb1694f65cce6132d9c9
    // ...


    static cv::String windowName("UWP sample");
    cv::winrt_initContainer(this->cvContainer);
    cv::namedWindow(windowName, CV_WINDOW_AUTOSIZE);

    string imageName("Assets/HappyFish.jpg");
    Mat image = imread(imageName, -1);

    if (image.empty()) {

        //Mat img(200, 400, CV_8UC3, Scalar(255, 0, 0));
        //imshow("Test Window", img);
        //waitKey(0);

        return;
    }

    cv::Mat converted = cv::Mat(image.rows, image.cols, CV_8UC4);
    cv::cvtColor(image, converted, COLOR_BGR2BGRA);
    cv::imshow(windowName, converted);
    // waitKey(0);


    // TBD:
    // Why do the trackbars not show up????
    // ...


    int state = 42;
    cv::TrackbarCallback callback = [](int pos, void* userdata)
    {
        if (pos == 0) {
            cv::destroyWindow(windowName);
        }
    };
    cv::TrackbarCallback callbackTwin = [](int pos, void* userdata)
    {
        if (pos >= 70) {
            cv::destroyAllWindows();
        }
    };
    cv::createTrackbar("Sample trackbar", windowName, &state, 100, callback);
    cv::createTrackbar("Twin brother", windowName, &state, 100, callbackTwin);


}
